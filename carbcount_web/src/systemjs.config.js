/**
 * System configuration for Angular samples
 * Adjust as necessary for your application needs.
 */
(function (global) {
  System.config({
    paths: {
      // paths serve as alias
      'npm:': 'node_modules/'
    },
    // map tells the System loader where to look for things
    map: {
      // our app is within the app folder
      'app': 'app',

      // angular bundles
      '@angular/core': 'npm:@angular/core/bundles/core.umd.js',
      '@angular/common': 'npm:@angular/common/bundles/common.umd.js',
      '@angular/compiler': 'npm:@angular/compiler/bundles/compiler.umd.js',
      '@angular/animations': 'node_modules/@angular/animations/bundles/animations.umd.min.js',
      '@angular/animations/browser': 'node_modules/@angular/animations/bundles/animations-browser.umd.js',
      '@angular/platform-browser': 'npm:@angular/platform-browser/bundles/platform-browser.umd.js',
      '@angular/platform-browser/animations': 'node_modules/@angular/platform-browser/bundles/platform-browser-animations.umd.js',
      '@angular/platform-browser-dynamic': 'npm:@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js',
      '@angular/http': 'npm:@angular/http/bundles/http.umd.js',
      '@angular/router': 'npm:@angular/router/bundles/router.umd.js',
      '@angular/forms': 'npm:@angular/forms/bundles/forms.umd.js',

      '@ngui/datetime-picker': 'npm:@ngui/datetime-picker/dist',
      '@ngx-bootstrap': 'npm:ngx-bootstrap',

      // other libraries
      'rxjs': 'npm:rxjs',
      'angular2-jwt': 'npm:angular2-jwt/angular2-jwt.js',
      'angular-calendar': 'npm:angular-calendar/dist/umd/angular-calendar.js',
      'calendar-utils': 'npm:calendar-utils/dist/umd/calendarUtils.js',
      'angular-resizable-element': 'npm:angular-resizable-element/dist/umd/angular-resizable-element.js',
      'angular-draggable-droppable': 'npm:angular-draggable-droppable/dist/umd/angular-draggable-droppable.js',
      'date-fns': 'npm:date-fns'
    },
    // packages tells the System loader how to load when no filename and/or no extension
    packages: {
      app: {
        defaultExtension: 'js',
        meta: {
          './*.js': {
            loader: 'systemjs-angular-loader.js'
          }
        }
      },
      rxjs: {
        defaultExtension: 'js'
      },
      'angular2-jwt': {
          defaultExtension: 'js'
      },
      '@ngui/datetime-picker': {
          main: 'datetime-picker.umd.js',
          defaultExtension: 'js'
      },
      'date-fns': {
          main: './index.js',
          defaultExtension: 'js'
      },
      '@ngx-bootstrap': {
          main: 'bundles/ngx-bootstrap.umd.js',
          defaultExtension: 'js'
      }
    }
  });
})(this);

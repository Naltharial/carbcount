﻿import { Component, OnInit, OnChanges } from '@angular/core';
import { Router } from '@angular/router';

import { User } from './user';
import { UserService } from './user.service';

@Component({
    selector: 'my-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit, OnChanges {
    title = 'CarbCounter';
    users: User[];
    selectedUser: User;

    onSelect(user: User): void {
        this.selectedUser = user;
    }
    getUsers(): void {
        this.userService.getUsers().then(users => this.users = users);
    }
    gotoDetail(): void {
        this.router.navigate(['/user', this.selectedUser.id]);
    }
    delete(user: User): void {
        this.userService
            .delete(user.id)
            .then(() => {
                this.users = this.users.filter(h => h !== user);
                if (this.selectedUser === user) { this.selectedUser = null; }
            });
    }

    ngOnInit(): void {
        this.getUsers();
    }
    ngOnChanges(): void {
        this.getUsers();
    }

    constructor(
        private router: Router,
        private userService: UserService
    ) { }
}

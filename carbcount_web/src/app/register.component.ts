﻿import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { User }    from './user';
import { AuthService } from './auth.service';

@Component({
    selector: 'register',
    templateUrl: './register.component.html'
})
export class RegisterComponent {
    model = new User();

    onSubmit() {
        this.authService.register(this.model)
            .then(
            result => this.router.navigate(['']),
            error => console.log(error)
            );
    }

    constructor(
        private router: Router,
        private authService: AuthService
    ) { }
}

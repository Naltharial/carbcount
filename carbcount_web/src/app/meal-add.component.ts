﻿import { Component, Input, Output, EventEmitter } from '@angular/core';

import { Meal }    from './meal';
import { MealService } from './meal.service';

@Component({
    selector: 'meal-add',
    templateUrl: './meal-add.component.html'
})
export class MealAddComponent {
    model = new Meal();
    @Output() mealAdded = new EventEmitter<Meal>();

    onSubmit() {
        this.mealService.create(this.model)
            .then(
            result => this.mealAdded.emit(this.model),
            error => console.log(error)
        );
    }

    constructor(
        private mealService: MealService
    ) { }
}

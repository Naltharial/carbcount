﻿import { Component, Output, EventEmitter } from '@angular/core';

import { User }    from './user';
import { UserService } from './user.service';

@Component({
    selector: 'user-add',
    templateUrl: './user-add.component.html'
})
export class UserAddComponent {
    model = new User();
    @Output() userAdded = new EventEmitter<User>();

    onSubmit() {
        this.userService.create(this.model)
            .then(
            result => this.userAdded.emit(this.model),
            error => console.log(error)
            );
    }

    constructor(
        private userService: UserService
    ) { }
}

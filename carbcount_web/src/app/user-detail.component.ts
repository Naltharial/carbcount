﻿import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';
import { Location }                 from '@angular/common';
import 'rxjs/add/operator/switchMap';

import { User } from './user';
import { UserService } from './user.service';

@Component({
    selector: 'user-detail',
    templateUrl: './user-detail.component.html',
    styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {
    @Input() user: User;

    goBack(): void {
        this.location.back();
    }
    save(): void {
        this.userService.update(this.user)
            .then(() => this.goBack());
    }

    ngOnInit(): void {
        this.route.params
            .switchMap((params: Params) => this.userService.getUser(+params['id']))
            .subscribe(user => this.user = user);
    }

    constructor(
        private userService: UserService,
        private route: ActivatedRoute,
        private location: Location
    ) { }
}

import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';
import { Location }                 from '@angular/common';
import 'rxjs/add/operator/switchMap';

import { Meal } from './meal';
import { MealService } from './meal.service';

@Component({
    selector: 'meal-detail',
    templateUrl: './meal-detail.component.html',
    styleUrls: ['./meal-detail.component.css']
})
export class MealDetailComponent implements OnInit {
    @Input() meal: Meal;

    goBack(): void {
        this.location.back();
    }
    save(): void {
        this.mealService.update(this.meal)
            .then(() => this.goBack());
    }

    ngOnInit(): void {
        this.route.params
            .switchMap((params: Params) => this.mealService.getMeal(+params['id']))
            .subscribe(meal => this.meal = meal);
    }

    constructor(
        private mealService: MealService,
        private route: ActivatedRoute,
        private location: Location
    ) { }
}

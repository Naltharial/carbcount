﻿import { Injectable }    from '@angular/core';
import { Headers } from '@angular/http';
import { AuthHttp } from 'angular2-jwt';

import 'rxjs/add/operator/toPromise';

import { User } from './user';

@Injectable()
export class UserService {
    private usersUrl = 'http://127.0.0.1:5000/api/user/';
    private headers = new Headers({ 'Content-Type': 'application/json' });

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

    getUsers(): Promise<User[]> {
        return this.http.get(this.usersUrl)
            .toPromise()
            .then(response => response.json().data as User[])
            .catch(this.handleError);
    }
    getUser(id: number): Promise<User> {
        const url = `${this.usersUrl}${id}`;
        return this.http.get(url)
            .toPromise()
            .then(response => response.json().data as User)
            .catch(this.handleError);
    }
    create(user: User): Promise<User> {
        return this.http
            .post(this.usersUrl, JSON.stringify({
                username: user.username,
                email: user.email,
                password: user.password,
                daily_target: +user.daily_target
            }), { headers: this.headers })
            .toPromise()
            .then(res => res.json().data as User)
            .catch(this.handleError);
    }
    update(user: User): Promise<User> {
        const url = `${this.usersUrl}${user.id}`;
        return this.http
            .patch(url, JSON.stringify(user), { headers: this.headers })
            .toPromise()
            .then(() => user)
            .catch(this.handleError);
    }
    delete(id: number): Promise<void> {
        const url = `${this.usersUrl}${id}`;
        return this.http.delete(url, { headers: this.headers })
            .toPromise()
            .then(() => null)
            .catch(this.handleError);
    }

    constructor(private http: AuthHttp) { }
}

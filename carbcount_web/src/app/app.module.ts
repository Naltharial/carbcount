import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpModule }    from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AuthHttp, AuthConfig, AUTH_PROVIDERS, provideAuth } from 'angular2-jwt';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';
import { CalendarModule } from 'angular-calendar';

import { AppRoutingModule }        from './app-routing.module';

import { AppComponent }        from './app.component';

import { MealDetailComponent } from './meal-detail.component';
import { MealsComponent }     from './meals.component';
import { MealAddComponent }     from './meal-add.component';

import { UserDetailComponent } from './user-detail.component';
import { UsersComponent }     from './users.component';
import { UserAddComponent }     from './user-add.component';

import { LoginComponent }     from './login.component';
import { ProfileComponent }     from './profile.component';
import { LogoutComponent }     from './logout.component';
import { RegisterComponent }     from './register.component';

import { MealService }         from './meal.service';
import { UserService }         from './user.service';
import { AuthService }         from './auth.service';
import { AuthGuard }         from './auth-guard.service';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,

        NguiDatetimePickerModule,
        BrowserAnimationsModule,
        CalendarModule.forRoot(),

        AppRoutingModule
    ],

    declarations: [
        AppComponent,

        MealDetailComponent,
        MealsComponent,
        MealAddComponent,

        UserDetailComponent,
        UsersComponent,
        UserAddComponent,

        LoginComponent,
        ProfileComponent,
        LogoutComponent,
        RegisterComponent
    ],

    providers: [
        AuthService,
        MealService,
        UserService,

        AuthHttp,
        provideAuth({
            headerName: 'Authorization',
            headerPrefix: 'bearer',
            tokenName: 'token',
            tokenGetter: (() => localStorage.getItem('id_token')),
            globalHeaders: [{ 'Content-Type': 'application/json' }],
            noJwtError: true
        }),
        AuthGuard
    ],

    bootstrap: [
        AppComponent
    ]
})
export class AppModule {
}

﻿export class Meal {
    id: number;
    description: string;
    calories: number;
    eaten_at: Date;

    user_id: number;
}

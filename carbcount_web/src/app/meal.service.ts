﻿import { Injectable }    from '@angular/core';
import { Headers } from '@angular/http';
import { AuthHttp } from 'angular2-jwt';

import 'rxjs/add/operator/toPromise';

import { Meal } from './meal';

@Injectable()
export class MealService {
    private mealsUrl = 'http://127.0.0.1:5000/api/meal/';
    private headers = new Headers({ 'Content-Type': 'application/json' });

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

    getMeals(startDate?: Date, endDate?: Date, startTime?: string, endTime?: string): Promise<Meal[]> {
        let url = this.mealsUrl;
        let params: string[] = [];
        if (startDate) params.push(`eaten_at__ge=${startDate.toISOString().split('.')[0] + "Z"}`)
        if (endDate) params.push(`eaten_at__le=${endDate.toISOString().split('.')[0] + "Z"}`)
        if (startTime) params.push(`eaten_at__time__ge=${startTime}`)
        if (endTime) params.push(`eaten_at__time__le=${endTime}`)
        if (params) url = url + '?' + params.join('&')
        
        return this.http.get(url)
            .toPromise()
            .then(response => response.json().data as Meal[])
            .catch(this.handleError);
    }
    getMeal(id: number): Promise<Meal> {
        const url = `${this.mealsUrl}${id}`;
        return this.http.get(url)
            .toPromise()
            .then(response => response.json().data as Meal)
            .catch(this.handleError);
    }
    create(meal: Meal): Promise<Meal> {
        return this.http
            .post(this.mealsUrl, JSON.stringify({
                description: meal.description,
                calories: meal.calories,
                eaten_at: new Date(meal.eaten_at).toISOString().split('.')[0] + "Z"
            }), { headers: this.headers })
            .toPromise()
            .then(res => res.json().data as Meal)
            .catch(this.handleError);
    }
    update(meal: Meal): Promise<Meal> {
        const url = `${this.mealsUrl}${meal.id}`;
        return this.http
            .patch(url, JSON.stringify({
                description: meal.description,
                calories: meal.calories,
                eaten_at: new Date(meal.eaten_at).toISOString().split('.')[0] + "Z"
            }), { headers: this.headers })
            .toPromise()
            .then(() => meal)
            .catch(this.handleError);
    }
    delete(id: number): Promise<void> {
        const url = `${this.mealsUrl}${id}`;
        return this.http.delete(url, { headers: this.headers })
            .toPromise()
            .then(() => null)
            .catch(this.handleError);
    }

    constructor(private http: AuthHttp) { }
}

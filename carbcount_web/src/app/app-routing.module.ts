import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from './auth-guard.service';

import { MealsComponent }      from './meals.component';
import { MealDetailComponent }  from './meal-detail.component';
import { UsersComponent }      from './users.component';
import { UserDetailComponent }  from './user-detail.component';
import { ProfileComponent }     from './profile.component';
import { LoginComponent }     from './login.component';
import { LogoutComponent }     from './logout.component';

const routes: Routes = [
    { path: '', redirectTo: '/meals', pathMatch: 'full' },

    { path: 'login', component: LoginComponent },
    { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
    { path: 'logout', component: LogoutComponent, canActivate: [AuthGuard] },

    { path: 'meal/:id', component: MealDetailComponent, canActivate: [AuthGuard] },
    { path: 'meals', component: MealsComponent, canActivate: [AuthGuard] },
    { path: 'user/:id', component: UserDetailComponent, canActivate: [AuthGuard] },
    { path: 'users', component: UsersComponent, canActivate: [AuthGuard] },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}

﻿import { Component } from '@angular/core';

import { AuthService } from './auth.service';

class Credentials {
    username: string;
    password: string;
}

@Component({
    selector: 'login',
    templateUrl: './login.component.html'
})

export class LoginComponent {
    credentials = new Credentials();

    onLogin() {
        this.auth.login(this.credentials);
    }

    constructor(private auth: AuthService) { }
}

﻿import { Component, Output, EventEmitter, OnInit } from '@angular/core';

import { AuthService } from './auth.service';

import { User }    from './user';

@Component({
    selector: 'profile',
    templateUrl: './profile.component.html'
})

export class ProfileComponent implements OnInit {
    model = new User();
    @Output() userChanged = new EventEmitter<User>();

    onSubmit() {
        this.auth.update(this.model)
            .then(
                result => this.userChanged.emit(this.model),
                error => console.log(error)
            );
    }

    ngOnInit() {
        this.auth.profile().then((profile: User) => this.model = profile);
    }

    constructor(private auth: AuthService) { }
}

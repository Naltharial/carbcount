import { Component, OnInit, OnChanges, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

import { Subject } from 'rxjs/Subject';

import { CalendarEvent, CalendarEventAction, CalendarMonthViewDay } from 'angular-calendar';
import {
    startOfDay,
    endOfDay,
    subDays,
    addDays,
    endOfMonth,
    isSameDay,
    isSameMonth,
    addHours
} from 'date-fns';

import { Meal } from './meal';
import { MealService } from './meal.service';

const colors: any = {
    red: {
        primary: '#ad2121',
        secondary: '#FAE3E3'
    },
    blue: {
        primary: '#1e90ff',
        secondary: '#D1E8FF'
    },
    yellow: {
        primary: '#e3bc08',
        secondary: '#FDF1BA'
    }
};

@Component({
    selector: 'my-meals',
    templateUrl: './meals.component.html',
    styleUrls: ['./meals.component.css'],
    encapsulation: ViewEncapsulation.None,
})
export class MealsComponent implements OnInit, OnChanges {
    title = 'CarbCounter';

    meals: Meal[];
    filter_date: any = {
        'start_date': null,
        'end_date': null,
        'start_time': null,
        'end_time': null,
    };
    dayCounters: any = {};

    view: string = 'month';
    viewDate: Date = new Date();
    refresh: Subject<any> = new Subject();
    activeDayIsOpen: boolean = false;
    addCssClass: (day: CalendarMonthViewDay) => void;


    events: CalendarEvent[];
    actions: CalendarEventAction[] = [{
        label: '<i class="fa fa-fw fa-pencil"></i>',
        onClick: ({event}: { event: CalendarEvent }): void => {
            this.eventClicked({ event: event });
        }
    }, {
        label: '<i class="fa fa-fw fa-times"></i>',
        onClick: ({event}: { event: CalendarEvent }): void => {
            this.eventDeleted({ event: event });
        }
    }];

    getMeals(): void {
        this.filter_date.start_date = new Date(this.viewDate.getFullYear(), this.viewDate.getMonth(), 1);
        this.filter_date.end_date = new Date(this.viewDate.getFullYear(), this.viewDate.getMonth() + 1, 0);
        this.filter_date.start_time = null;
        this.filter_date.end_time = null;
        this.activeDayIsOpen = false;

        this.mealService.getMeals(
            this.filter_date.start_date,
            this.filter_date.end_date
        ).then((meals: Meal[]) => this._applyMeals(meals));
    }
    getFilteredMeals(): void {
        this.mealService.getMeals(
            new Date(this.filter_date.start_date),
            new Date(this.filter_date.end_date),
            this.filter_date.start_time,
            this.filter_date.end_time
        ).then((meals: Meal[]) => this._applyMeals(meals));
    }
    eventClicked({event}: { event: CalendarEvent }): void {
        let selectedMeal = this.meals.filter(
            meal => meal.description + ', ' + meal.calories + ' cal' === event.title
        )[0];
        this.router.navigate(['meal', selectedMeal.id]);
    }
    eventDeleted({event}: { event: CalendarEvent }): void {
        let selectedMeal = this.meals.filter(
            meal => meal.description + ', ' + meal.calories + ' cal' === event.title
        )[0];
        this.mealService.delete(selectedMeal.id).then(() => this.getMeals());
    }
    dayClicked({date, events}: { date: Date, events: CalendarEvent[] }): void {
        if (isSameMonth(date, this.viewDate)) {
            if (
                (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
                events.length === 0
            ) {
                this.activeDayIsOpen = false;
            } else {
                this.activeDayIsOpen = true;
                this.viewDate = date;
            }
        }
    }

    ngOnInit(): void {
        this.getMeals();
    }
    ngOnChanges(changes?: SimpleChanges) {
        this.getMeals();
    }

    _applyMeals(meals: Meal[]) {
        let events: CalendarEvent[] = [];
        this.meals = meals;
        this.dayCounters = {};

        for (var m in this.meals) {
            let meal = this.meals[m];
            let date = new Date(meal.eaten_at)
            let cdate = date.getFullYear() + '-' + date.getMonth() + 1 + '-' + date.getDate();
            this.dayCounters[cdate] = this.dayCounters[cdate] ? this.dayCounters[cdate] + meal.calories : meal.calories;

            let event: CalendarEvent = {
                title: meal.description + ', ' + meal.calories + ' cal',
                color: colors.blue,
                start: date,
                end: date
            }
            events.push(event);
        }

        this.events = events;
        this.refresh.next();
    }

    constructor(
        private router: Router,
        private mealService: MealService
    ) {
        this.addCssClass = (day: CalendarMonthViewDay): void => {
            let target: number = +localStorage.getItem('daily_target');
            if (day.inMonth) {
                let cdate = day.date.getFullYear() + '-' + day.date.getMonth() + 1 + '-' + day.date.getDate();
                let ccal = this.dayCounters[cdate];
                if (ccal) {
                    if (ccal <= target) {
                        day.cssClass = 'under-cell';
                    } else if (ccal > target) {
                        day.cssClass = 'over-cell';
                    }
                }
            }
        };
    }
}

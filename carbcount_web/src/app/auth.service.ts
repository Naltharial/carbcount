﻿import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Router } from '@angular/router';
import { AuthHttp } from 'angular2-jwt';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/toPromise';

import { User }    from './user';

@Injectable()
export class AuthService {
    url = "http://127.0.0.1:5000/auth/";
    private headers = new Headers({ 'Content-Type': 'application/json' });

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

    login(credentials: { username: string, password: string }) {
        this.http.post(this.url+"login/", credentials)
            .map(res => res.json())
            .subscribe(
            data => {
                    localStorage.setItem('roles', data.roles);
                    localStorage.setItem('daily_target', data.daily_target);
                    localStorage.setItem('id_token', data.access_token);
                    this.router.navigate(['']);
                },
                error => {
                    console.log(error);
                }
            );
    }
    logout() {
        localStorage.removeItem('id_token');
    }
    register(user: User): Promise<User> {
        return this.http
            .post(this.url+"register/", JSON.stringify({
                username: user.username,
                email: user.email,
                password: user.password,
                daily_target: +user.daily_target
            }), { headers: this.headers })
            .toPromise()
            .then(res => res.json().data as User)
            .catch(this.handleError);
    }
    profile(): Promise<User> {
        return this.authHttp
            .get(this.url+"profile/")
            .toPromise()
            .then(res => res.json().data as User)
            .catch(this.handleError);
    }
    update(user: User) {
        return this.authHttp
            .patch(this.url + "profile/", JSON.stringify({
                email: user.email,
                daily_target: +user.daily_target
            }), { headers: this.headers })
            .toPromise()
            .then(res => {
                this.router.navigate(['']);
                localStorage.setItem('daily_target', `${user.daily_target}`);
            })
            .catch(this.handleError);
    }

    loggedIn() {
        return localStorage.getItem('id_token') !== null;
    }

    constructor(
        private router: Router,
        private http: Http,
        private authHttp: AuthHttp
    ) { }
}

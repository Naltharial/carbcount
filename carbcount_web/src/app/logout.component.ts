﻿import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from './auth.service';

@Component({
    selector: 'login',
    template: ''
})

export class LogoutComponent implements OnInit {
    ngOnInit() {
        this.auth.logout();
        this.router.navigate(['']);
    }

    constructor(
        private router: Router,
        private auth: AuthService
    ) { }
}

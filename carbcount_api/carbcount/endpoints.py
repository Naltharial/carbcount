# -*- coding: utf-8 -*-
import importlib
import inspect
import pkgutil

from falcon_autocrud.resource import BaseResource

# Set to None to disable auto-discovery
app_models = True
if app_models:
    # Location of SQLAlchemy models
    import carbcount.api as app_models


def register_endpoints(endpoints):
    if app_models:
        for importer, modname, is_pkg in pkgutil.iter_modules(app_models.__path__):
            m = importlib.import_module(app_models.__name__ + "." + modname)
            for cls_name, cls_obj in inspect.getmembers(m):
                if inspect.isclass(cls_obj) and BaseResource in cls_obj.__mro__:
                    endpoints[cls_name] = cls_obj

all_views = {
    'LoginResource': 'carbcount.views.user',
    'RegisterResource': 'carbcount.views.user',

    'ProfileResource': 'carbcount.views.user',
}

all_endpoints = {}

register_endpoints(all_endpoints)

# Custom imports
#
# from package import module
# all_endpoints[module.cls_name] = module.cls_obj

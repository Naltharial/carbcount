# -*- coding: utf-8 -*-
"""Convenience functions which interact with SQLAlchemy models."""

import sqlalchemy
import sqlalchemy_utils
from sqlalchemy import func
from sqlalchemy.sql import operators

QUERY_SEP = "__"

_oper = {
    'eq': operators.eq,
    'gt': operators.gt,
    'lt': operators.lt,
    'ge': operators.ge,
    'le': operators.le,
    'ne': operators.ne,
    'contains': operators.contains_op,
}


class DefaultMixin(object):
    __table_args__ = dict(mysql_charset='utf8', mysql_engine='InnoDB')

    id = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True, autoincrement=True, nullable=True)

    _created = sqlalchemy.Column(sqlalchemy_utils.ArrowType, default=func.now())
    _updated = sqlalchemy.Column(sqlalchemy_utils.ArrowType, default=func.now(), onupdate=func.now())
    _etag = sqlalchemy.Column(sqlalchemy.String(40))

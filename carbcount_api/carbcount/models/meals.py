# -*- coding: utf-8 -*-
# App Imports
import sqlalchemy
import sqlalchemy_utils
from sqlalchemy.orm import relationship, backref

from carbcount.models.helpers import DefaultMixin
from carbcount.extensions import DeclarativeBase


class Meal(DeclarativeBase, DefaultMixin):
    __tablename__ = "meals"

    description = sqlalchemy.Column(sqlalchemy.String(255), nullable=False)
    calories = sqlalchemy.Column(sqlalchemy.Integer, nullable=False)
    eaten_at = sqlalchemy.Column(sqlalchemy_utils.ArrowType, nullable=False)

    user_id = sqlalchemy.Column(sqlalchemy.Integer, sqlalchemy.ForeignKey('users.id', name='fk_meal_user'))
    user = relationship("User", backref=backref("meals"))

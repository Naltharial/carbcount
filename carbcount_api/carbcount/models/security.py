# -*- coding: utf-8 -*-
import os
import bcrypt
import struct
import passlib.utils

import sqlalchemy
from sqlalchemy.orm import relationship, backref

# App Imports
from carbcount.models.helpers import DefaultMixin
from carbcount.extensions import DeclarativeBase

user_roles = sqlalchemy.Table('user_roles', DeclarativeBase.metadata,
                              sqlalchemy.Column('user_id', sqlalchemy.Integer, sqlalchemy.ForeignKey('users.id')),
                              sqlalchemy.Column('role_id', sqlalchemy.Integer, sqlalchemy.ForeignKey('roles.id')))


class Role(DeclarativeBase, DefaultMixin):
    __tablename__ = "roles"

    class Meta:
        resource_methods = []

    name = sqlalchemy.Column(sqlalchemy.String(80), unique=True)
    description = sqlalchemy.Column(sqlalchemy.String(255))


class User(DeclarativeBase, DefaultMixin):
    __tablename__ = "users"

    _PARAMS = struct.Struct("!BBB")

    class Meta:
        allowed_filters = ['email']

    email = sqlalchemy.Column(sqlalchemy.String(255), unique=True)
    username = sqlalchemy.Column(sqlalchemy.String(255), nullable=False)
    password = sqlalchemy.Column(sqlalchemy.String(255), nullable=False)
    active = sqlalchemy.Column(sqlalchemy.Boolean)
    daily_target = sqlalchemy.Column(sqlalchemy.Integer, nullable=False, default=0)

    roles = relationship('Role', secondary=user_roles,
                         backref=backref('users', lazy='dynamic'))

    def has_role(self, role):
        return role in self.roles

    def is_authorized(self, role_names):
        if not role_names:
            return True
        for role in role_names:
            if not self.has_role(role):
                return False
        return True

    def verify_password(self, password, pepper=''):
        password = str(password).encode('utf-8')
        pepper = pepper.encode('utf-8')

        rounds, salt, pass_hash = self.unpack_verifier(self.password)
        new_hash = bcrypt.kdf(password, salt + pepper, len(pass_hash), rounds)

        return passlib.utils.consteq(new_hash, pass_hash)

    @classmethod
    def pack_verifier(cls, rounds, salt, pass_hash):
        packed = cls._PARAMS.pack(rounds, len(salt), len(pass_hash)) + salt + pass_hash
        return packed

    @classmethod
    def unpack_verifier(cls, verifier):
        rounds, salt_bytes, hash_bytes = cls._PARAMS.unpack_from(verifier)
        i = cls._PARAMS.size + salt_bytes
        salt = verifier[cls._PARAMS.size:i]
        pass_hash = verifier[i:]
        return rounds, salt, pass_hash

    @classmethod
    def make_verifier(cls, password, salt_bytes=16, hash_bytes=32, rounds=100, pepper=''):
        password = str(password).encode('utf-8')
        salt = str(os.urandom(salt_bytes)).encode('ascii')
        pepper = pepper.encode('utf-8')

        pass_hash = bcrypt.kdf(password, salt + pepper, hash_bytes, rounds)
        return cls.pack_verifier(rounds, salt, pass_hash)

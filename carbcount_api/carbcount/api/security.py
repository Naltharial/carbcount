from falcon_autocrud.auth import identify, authorize

from carbcount.core import auth, resource
from carbcount.models import security


@identify(auth.Identifier)
@authorize(auth.AdminAuthorizer)
@authorize(auth.ManagerAuthorizer, methods=['GET'])
class RoleCollectionResource(resource.CollectionResource):
    resource_uri = '/api/role'
    methods = ['GET']
    model = security.Role


@identify(auth.Identifier)
@authorize(auth.AdminAuthorizer)
@authorize(auth.ManagerAuthorizer, methods=['GET'])
class RoleResource(resource.SingleResource):
    resource_uri = '/api/role/{id}'
    methods = ['GET']
    model = security.Role


@identify(auth.Identifier)
@authorize(auth.ManagerAuthorizer)
class UserCollectionResource(resource.CollectionResource):
    resource_uri = '/api/user'
    model = security.User

    response_fields = ['id', 'username', 'email', 'daily_target']

    def before_post(self, req, resp, db_session, resource, *args, **kwargs):
        resource.password = security.User.make_verifier(resource.password)


@identify(auth.Identifier)
@authorize(auth.ManagerAuthorizer)
class UserResource(resource.SingleResource):
    resource_uri = '/api/user/{id}'
    model = security.User

    response_fields = ['id', 'username', 'email', 'daily_target']

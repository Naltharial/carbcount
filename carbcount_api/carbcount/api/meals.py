from sqlalchemy import and_, extract

from falcon import HTTPForbidden, HTTPBadRequest
from falcon_autocrud.auth import identify, authorize

from carbcount.core import auth, resource
from carbcount.models import meals, security as security_models, helpers


@identify(auth.Identifier)
@authorize(auth.Authorizer)
class MealCollectionResource(resource.CollectionResource):
    resource_uri = '/api/meal'
    model = meals.Meal

    def before_post(self, req, resp, db_session, resource, *args, **kwargs):
        if not resource.user:
            user_obj = db_session.query(security_models.User).filter(
                security_models.User.username == req.context['user']
            ).one()
            resource.user = user_obj

    def get_filter(self, req, resp, query, *args, **kwargs):
        adm = auth.AdminAuthorizer()
        try:
            adm.authorize(req, resp, self, args)
        except HTTPForbidden:
            return query.filter(and_(
                security_models.User.id == meals.Meal.user_id,
                security_models.User.username == req.context['user']
            ))

        return query

    def filter_by_params(self, resources, params):
        param_list = list(params.items())
        real_param = []
        for i in range(len(param_list)):
            name, value = param_list[i]
            name = self.deep_parse_key(name)
            toks = name.split(resource.QUERY_SEP)
            if 'eaten_at' in toks and 'time' in toks:
                op = helpers._oper.get(toks[len(toks)-1], helpers._oper['eq'])
                ttok = value.split(':')
                if len(ttok) == 2:
                    resources = resources.filter(
                        op(extract('HOUR_MINUTE', meals.Meal.eaten_at), ''.join(ttok)),
                    )
                else:
                    raise HTTPBadRequest(
                        'Invalid operation',
                        'An operation provided for filtering is invalid'
                    )
            else:
                real_param.append(param_list[i])

        return super().filter_by_params(resources, dict(real_param))


@identify(auth.Identifier)
@authorize(auth.Authorizer)
class MealResource(resource.SingleResource):
    resource_uri = '/api/meal/{id}'
    model = meals.Meal

    def after_get(self, req, resp, resource, *args, **kwargs):
        if resource.user.username != req.context['user']:
            adm = auth.AdminAuthorizer()
            try:
                adm.authorize(req, resp, self, args)
            except HTTPForbidden:
                req.context['result'] = {}
                raise

from falcon import HTTPUnauthorized, HTTPForbidden
from falcon_autocrud.db_session import session_scope

from carbcount.views import verify_auth_token
from carbcount.models import security as security_models


class Identifier(object):
    def identify(self, req, resp, resource, params):
        token = req.auth
        if not token:
            raise HTTPUnauthorized('Authentication Required', 'No credentials supplied', None)

        tokens = token.split(" ")
        if len(tokens) < 2:
            raise HTTPUnauthorized('Authentication Required', 'Invalid credentials supplied', None)

        req.context['user'] = verify_auth_token(tokens[1], req)
        if req.context['user'] is None:
            raise HTTPUnauthorized('Authentication Required', 'Invalid credentials supplied', None)


class Authorizer(object):
    def authorize(self, req, resp, resource, params):
        if 'user' not in req.context:
            raise HTTPForbidden('Permission Denied', 'User does not have access to this resource')


class ManagerAuthorizer(Authorizer):
    def authorize(self, req, resp, resource, params):
        super().authorize(req, resp, resource, params)

        with session_scope(resource.db_engine) as db_session:
            user_obj = db_session.query(security_models.User).filter(
                security_models.User.username == req.context['user']
            ).one()

            roles = [role.name for role in user_obj.roles]
            if 'Manager' not in roles and 'Admin' not in roles:
                raise HTTPForbidden('Permission Denied', 'User does not have access to this resource')


class AdminAuthorizer(Authorizer):
    def authorize(self, req, resp, resource, params):
        super().authorize(req, resp, resource, params)

        with session_scope(resource.db_engine) as db_session:
            user_obj = db_session.query(security_models.User).filter(
                security_models.User.username == req.context['user']
            ).one()

            roles = [role.name for role in user_obj.roles]
            if 'Admin' not in roles:
                raise HTTPForbidden('Permission Denied', 'User does not have access to this resource')

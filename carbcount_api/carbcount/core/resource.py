import abc

from sqlalchemy.orm.base import _entity_descriptor
from sqlalchemy.sql import operators

import falcon
from falcon_autocrud import resource


QUERY_SEP = "__"

_oper = {
    'eq': operators.eq,
    'gt': operators.gt,
    'lt': operators.lt,
    'ge': operators.ge,
    'le': operators.le,
    'ne': operators.ne,
    'contains': operators.contains_op,
}


class FilterMixin(object):
    __metaclass__ = abc.ABCMeta

    def filter_by_params(self, resources, params):
        for name, value in params.items():
            col = None
            op = _oper['eq']
            name = self.deep_parse_key(name)
            tokens = name.split(QUERY_SEP)
            for tok in tokens:
                if col is None:
                    col = _entity_descriptor(resources._joinpoint_zero(), tok)
                    try:
                        resources = resources.join(col, aliased=True, from_joinpoint=True)
                        col = None
                    except AttributeError:
                        pass
                elif tok in _oper:
                    if col is None:
                        raise falcon.errors.HTTPBadRequest(
                            'Invalid operation',
                            'An operation provided for filtering is invalid'
                        )
                    op = _oper[tok]
                else:
                    raise falcon.errors.HTTPBadRequest(
                        'Invalid attribute',
                        'An attribute provided for filtering is invalid'
                    )

            resources = resources.filter(op(col, value))

        return resources

    @staticmethod
    def deep_parse_key(key):
        return key.replace('[', '__').replace(']', '')


class CollectionResource(FilterMixin, resource.CollectionResource):
    pass


class SingleResource(FilterMixin, resource.SingleResource):
    pass

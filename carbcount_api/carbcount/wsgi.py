# -*- coding: utf-8 -*-
from carbcount.application import create_app, get_config

app = create_app(get_config('carbcount.config.Heroku'))

if __name__ == "__main__":
    app.run()

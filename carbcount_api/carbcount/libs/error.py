import falcon


class APIError(falcon.HTTPError):
    def __init__(self, error=falcon.HTTP_NOT_FOUND, description=None):
        super().__init__(status=error, description=description)


class NotSupportedError(APIError):
    def __init__(self, method=None, url=None):
        description = None
        if method and url:
            description = 'method: %s, url: %s' % (method, url)
        super().__init__(falcon.HTTP_NOT_IMPLEMENTED, description)


class UnauthorizedError(APIError):
    def __init__(self, description=None):
        super().__init__(falcon.HTTP_UNAUTHORIZED, description)

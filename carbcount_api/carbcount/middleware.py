import arrow


class ConfigMiddleware(object):
    def __init__(self, config):
        self.config = config

    def process_request(self, req, resp):
        req.context['config'] = self.config


class VaryMiddleware(object):
    def __init__(self, headers, replace=False):
        self.headers = headers
        self.replace = replace

    def process_response(self, req, resp, resource):
        vary = ", ".join(self.headers)
        if resp._headers.get('Vary', None) and not self.replace:
            resp._headers['Vary'] += ", " + vary
        else:
            resp._headers['Vary'] = vary


class DateTimeMiddleware(object):
    def process_response(self, req, resp, resource):
        result = req.context.get('result', None)
        if result:
            data = req.context['result'].get('data', None)
            if data:
                if isinstance(data, dict):
                    for name, value in data.items():
                        if isinstance(value, arrow.Arrow):
                            req.context['result']['data'][name] = value.isoformat()
                else:
                    for i, item in enumerate(data):
                        for name, value in item.items():
                            if isinstance(value, arrow.Arrow):
                                req.context['result']['data'][i][name] = value.isoformat()

# -*- coding: utf-8 -*-
from importlib import import_module
import os
from yaml import load

from sqlalchemy import create_engine

import falcon
import falcon_cors
import falconjsonio.middleware

import carbcount as app_root
import carbcount.middleware
from carbcount.endpoints import all_views, all_endpoints

APP_ROOT_FOLDER = os.path.abspath(os.path.dirname(app_root.__file__))
STATIC_FOLDER = os.path.join(APP_ROOT_FOLDER, 'static')
REDIS_SCRIPTS_FOLDER = os.path.join(APP_ROOT_FOLDER, 'redis_scripts')


def get_config(config_class_string, yaml_files=None):
    """Load the config from a class.

    Positional arguments:
    config_class_string -- string representation of a configuration class that will be loaded (e.g.
        'carbcount.config.Production').
    yaml_files -- List of YAML files to load. This is for testing, leave None in dev/production.

    Returns:
    A class object to be fed into app.config.from_object().
    """
    config_module, config_class = config_class_string.rsplit('.', 1)
    config_class_object = getattr(import_module(config_module), config_class)
    config_obj = config_class_object()

    # Load additional configuration settings.
    yaml_files = yaml_files or [f for f in [
        os.path.join(APP_ROOT_FOLDER, 'config.yml'),
        # More paths ...
    ] if os.path.exists(f)]
    additional_dict = dict()
    for y in yaml_files:
        with open(y) as f:
            additional_dict.update(load(f.read()))

    # Merge the rest into the app config.
    for key, value in additional_dict.items():
        setattr(config_obj, key, value)

    return config_obj


def create_app(config_obj):
    from sqlalchemy.pool import NullPool
    db_engine = create_engine(
        "%s?charset=utf8" % config_obj.SQLALCHEMY_DATABASE_URI,
        poolclass=NullPool
    )

    cors = falcon_cors.CORS(
        allow_all_origins=True,
        allow_all_methods=True,
        allow_headers_list=config_obj.X_HEADERS
    )
    app = falcon.API(
            middleware=[
                falconjsonio.middleware.RequireJSON(),
                falconjsonio.middleware.JSONTranslator(),
                carbcount.middleware.ConfigMiddleware(config_obj),
                carbcount.middleware.VaryMiddleware(['Authorization']),
                carbcount.middleware.DateTimeMiddleware(),
                cors.middleware,
            ],
        )

    for cls_name, cls_obj in all_endpoints.items():
        app.add_route(cls_obj.resource_uri, cls_obj(db_engine))

    for cls_name, cls_module in all_views.items():
        mod = import_module(cls_module)
        cls_obj = getattr(mod, cls_name)
        app.add_route(cls_obj.resource_uri, cls_obj(db_engine))

    return app

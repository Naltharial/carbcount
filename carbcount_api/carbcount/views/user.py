# -*- coding: utf-8 -*-
import os
from passlib.hash import pbkdf2_sha512

from cerberus import Validator

import falcon
from falcon_autocrud.db_session import session_scope
from falcon_autocrud.auth import identify, authorize
from falcon_autocrud.resource import identify as id_search, authorize as au_search

from carbcount.core import auth

from carbcount.libs import error
from carbcount.models import security as security_models
from carbcount.views import ViewResource, get_auth_token

FIELDS = {
    'username': {
        'type': 'string',
        'required': True,
        'minlength': 4,
        'maxlength': 20
    },
    'email': {
        'type': 'string',
        'regex': '[a-zA-Z0-9._-]+@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,4}',
        'required': True,
        'maxlength': 320
    },
    'password': {
        'type': 'string',
        'regex': '[0-9a-zA-Z]\w{3,14}',
        'required': True,
        'minlength': 8,
        'maxlength': 64
    },
    'daily_target': {
        'type': 'number',
        'required': False
    }
}


def validate_user_create(req, res, resource, params):
    v = Validator(FIELDS)
    if not v.validate(req.context['doc']):
        raise error.APIError(falcon.HTTP_UNPROCESSABLE_ENTITY, description=v.errors)


class LoginResource(ViewResource):
    resource_uri = '/auth/login'
    methods = ['POST']

    def on_post(self, req, resp, *args, **kwargs):
        with session_scope(self.db_engine) as db_session:
            attributes = req.context['doc']
            username = attributes.get('username')
            password = attributes.get('password')

            if not username or not password:
                raise falcon.HTTPUnauthorized('Authentication Required', 'Missing username and/or password.', None)
            else:
                user = db_session.query(security_models.User).filter(security_models.User.username == username).first()
                if not user:
                    raise falcon.HTTPUnauthorized('Authentication Required', 'Missing username and/or password.', None)

                vpass = user.verify_password(password)
                if user and vpass:
                    token = get_auth_token(
                        {
                            "username": username,
                            "password": pbkdf2_sha512.encrypt(os.urandom(8))
                        },
                        req
                    )

                    resp.status = falcon.HTTP_OK
                    req.context['result'] = {
                        'username': user.username,
                        'email': user.email,
                        'daily_target': user.daily_target,
                        'roles': [role.name for role in user.roles],
                        'access_token': token.decode('ascii')
                    }
                else:
                    raise falcon.HTTPUnauthorized('Authentication Required', 'Wrong username and/or password.', None)


class RegisterResource(ViewResource):
    resource_uri = '/auth/register'
    methods = ['POST']

    @falcon.before(validate_user_create)
    def on_post(self, req, resp, *args, **kwargs):
        with session_scope(self.db_engine) as db_session:
            attributes = req.context['doc']

            user = security_models.User(
                username=attributes['username'],
                email=attributes['email'],
                password=security_models.User.make_verifier(attributes['password']),
            )
            if attributes.get('daily_target', None):
                user.daily_target = int(attributes['daily_target'])

            db_session.add(user)
            db_session.commit()

            resp.status = falcon.HTTP_OK


@identify(auth.Identifier)
@authorize(auth.Authorizer)
class ProfileResource(ViewResource):
    resource_uri = '/auth/profile'
    methods = ['GET', 'PATCH']

    @falcon.before(id_search)
    @falcon.before(au_search)
    def on_get(self, req, resp):
        with session_scope(self.db_engine) as db_session:
            user = db_session.query(security_models.User).filter(
                security_models.User.username == req.context['user']
            ).one()

            resp.status = falcon.HTTP_OK
            req.context['result'] = {
                'data': {
                    'username': user.username,
                    'email': user.email,
                    'daily_target': user.daily_target,
                }
            }

    @falcon.before(id_search)
    @falcon.before(au_search)
    def on_patch(self, req, resp):
        with session_scope(self.db_engine) as db_session:
            attributes = req.context['doc']

            user = db_session.query(security_models.User).filter(
                security_models.User.username == req.context['user']
            ).one()
            if attributes.get('email', None):
                user.email = attributes['email']
            if attributes.get('daily_target', None):
                user.daily_target = int(attributes['daily_target'])

            db_session.add(user)
            db_session.commit()

            resp.status = falcon.HTTP_OK

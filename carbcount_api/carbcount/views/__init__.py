import logging
import itsdangerous

from sqlalchemy.orm import sessionmaker


def get_auth_token(data, req, expiration=24*60*60):
    s = itsdangerous.TimedJSONWebSignatureSerializer(req.context['config'].SECRET_KEY, expires_in=expiration)
    ret = s.dumps(data)
    return ret


def verify_auth_token(token, req, expiration=24*60*60):
    s = itsdangerous.TimedJSONWebSignatureSerializer(req.context['config'].SECRET_KEY, expires_in=expiration)
    try:
        data = s.loads(token)
    except itsdangerous.SignatureExpired:
        return None
    except itsdangerous.BadSignature:
        return None
    return data['username']


class ViewResource(object):
    def __init__(self, db_engine, logger=None, sessionmaker_=sessionmaker, sessionmaker_kwargs=None):
        if not sessionmaker_kwargs:
            sessionmaker_kwargs = {}

        self.db_engine = db_engine
        self.sessionmaker = sessionmaker_
        self.sessionmaker_kwargs = sessionmaker_kwargs
        if logger is None:
            logger = logging.getLogger('autocrud')
        self.logger = logger

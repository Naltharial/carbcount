# -*- coding: utf-8 -*-
"""
Commands:
    devserver
    init

Usage:
    manage.py devserver [--config=<env>]
    manage.py db [--config=<env>] <commands>...
    manage.py init
    manage.py (-h | --help)

Options:
    --config=<env>                          Load the selected configuration instead of development.
"""
from functools import wraps
from docopt import docopt
import logging
import logging.handlers
import os
import signal
import sys

from waitress import serve

import alembic.config
from sqlalchemy import create_engine

from falcon_autocrud.db_session import session_scope

from carbcount.application import create_app, get_config
from carbcount.models import security as security_models

OPTIONS = docopt(__doc__)


class CustomFormatter(logging.Formatter):
    LEVEL_MAP = {logging.FATAL: 'F', logging.ERROR: 'E', logging.WARN: 'W', logging.INFO: 'I', logging.DEBUG: 'D'}

    def format(self, record):
        record.levelletter = self.LEVEL_MAP[record.levelno]
        return super(CustomFormatter, self).format(record)


def setup_logging(name=None):
    log_to_disk = False
    if OPTIONS.get('--log_dir', '.'):
        if not os.path.isdir(OPTIONS.get('--log_dir', '.')):
            print('ERROR: Directory {} does not exist.'.format(OPTIONS.get('--log_dir', '.')))
            sys.exit(1)
        if not os.access(OPTIONS.get('--log_dir', '.'), os.W_OK):
            print('ERROR: No permissions to write to directory {}.'.format(OPTIONS.get('--log_dir', '.')))
            sys.exit(1)
        log_to_disk = True

    fmt = '%(levelletter)s%(asctime)s.%(msecs).03d %(process)d %(filename)s:%(lineno)d] %(message)s'
    datefmt = '%m%d %H:%M:%S'
    formatter = CustomFormatter(fmt, datefmt)

    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setLevel(logging.ERROR if log_to_disk else logging.DEBUG)
    console_handler.setFormatter(formatter)

    root = logging.getLogger()
    root.setLevel(logging.DEBUG)
    root.addHandler(console_handler)

    if log_to_disk:
        file_name = os.path.join(OPTIONS.get('--log_dir', '.'), 'carbcount_{}.log'.format(name))
        file_handler = logging.handlers.TimedRotatingFileHandler(file_name, when='d', backupCount=7)
        file_handler.setFormatter(formatter)
        root.addHandler(file_handler)


def log_messages(app, port):
    log = logging.getLogger(__name__)
    log.info('Server is running at http://0.0.0.0:{}/'.format(port))


def parse_options():
    """Parses command line options for Flask.

    Returns:
    Config instance to pass into create_app().
    """
    # Figure out which class will be imported.
    if OPTIONS.get('--config', None):
        config_class_string = 'carbcount.config.%s' % OPTIONS['--config']
    else:
        config_class_string = 'carbcount.config.Config'
    config_obj = get_config(config_class_string)

    return config_obj


def command(func):
    @wraps(func)
    def wrapped():
        return func()

    if __name__ == '__main__':
        if func.__name__ not in OPTIONS:
            raise KeyError('Cannot register {}, not mentioned in docstring/docopt.'.format(func.__name__))
        if OPTIONS[func.__name__]:
            command.chosen = func

    return wrapped


@command
def db():
    setup_logging('db')
    cmds = OPTIONS['<commands>']
    cmds.insert(0, 'migrations/alembic.ini')
    cmds.insert(0, '-c')
    alembic.config.main(argv=cmds)


@command
def devserver():
    setup_logging('devserver')
    app = create_app(parse_options())
    log_messages(app, OPTIONS.get('--port', '5000'))
    serve(app, host='0.0.0.0', port=int(OPTIONS.get('--port', '5000')))

@command
def init():
    config_obj = parse_options()
    db_engine = create_engine(config_obj.SQLALCHEMY_DATABASE_URI)
    with session_scope(db_engine) as db_session:
        role = db_session.query(security_models.Role).filter(security_models.Role.name == "Admin").first()
        if not role:
            role = security_models.Role(
                name="Admin",
                description="CarbCount Administrator"
            )
            db_session.add(role)
            rolem = security_models.Role(
                name="Manager",
                description="CarbCount User Manager"
            )
            db_session.add(rolem)
            db_session.commit()

        if db_session.query(security_models.User).count() == 0:
            user = security_models.User(
                username='admin',
                email='dev@null.test',
                password=security_models.User.make_verifier("admin")
            )
            user.roles.append(role)
        db_session.commit()

if __name__ == '__main__':
    signal.signal(signal.SIGINT, lambda *_: sys.exit(0))  # Properly handle Control+C

    if not OPTIONS.get('--port', '0').isdigit():
        print('ERROR: Port should be a number.')
        sys.exit(1)

    getattr(command, 'chosen')()  # Execute the function specified by the user.

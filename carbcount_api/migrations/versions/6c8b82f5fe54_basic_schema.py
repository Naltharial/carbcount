"""Basic schema

Revision ID: 6c8b82f5fe54
Revises: None
Create Date: 2017-03-08 17:32:01.454214

"""

# revision identifiers, used by Alembic.
revision = '6c8b82f5fe54'
down_revision = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('users',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('email', sa.String(length=80), nullable=False),
        sa.Column('password', sa.BLOB, nullable=False),
        sa.Column('username', sa.String(length=80), nullable=False),
        sa.Column('daily_target', sa.Integer, nullable=False, server_default='0'),
        sa.Column('active', sa.Boolean, default=False, server_default='0'),
        sa.Column('_created', sa.DateTime),
        sa.Column('_updated', sa.DateTime),
        sa.Column('_etag', sa.String(length=40)),
        sa.PrimaryKeyConstraint('id'),
        mysql_charset='utf8',
        mysql_engine='InnoDB'
    )

    op.create_table('roles',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('name', sa.String(length=80), nullable=False),
        sa.Column('description', sa.Text, nullable=False),
        sa.Column('_created', sa.DateTime),
        sa.Column('_updated', sa.DateTime),
        sa.Column('_etag', sa.String(length=40)),
        sa.PrimaryKeyConstraint('id'),
        mysql_charset='utf8',
        mysql_engine='InnoDB'
    )

    op.create_table('user_roles',
        sa.Column('user_id', sa.Integer),
        sa.Column('role_id', sa.Integer),
        sa.ForeignKeyConstraint(['user_id'], ['users.id'], name='fk_userroles_user',
                                onupdate='CASCADE', ondelete='CASCADE'),
        sa.ForeignKeyConstraint(['role_id'], ['roles.id'], name='fk_userroles_role',
                                onupdate='CASCADE', ondelete='CASCADE'),
        mysql_charset='utf8',
        mysql_engine='InnoDB'
    )

    op.create_table('meals',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('description', sa.Text, nullable=False),
        sa.Column('calories', sa.Integer, nullable=False),
        sa.Column('eaten_at', sa.DateTime, nullable=False),
        sa.Column('user_id', sa.Integer, nullable=False),
        sa.ForeignKeyConstraint(['user_id'], ['users.id'], name='fk_meal_user'),
        sa.Column('_created', sa.DateTime),
        sa.Column('_updated', sa.DateTime),
        sa.Column('_etag', sa.String(length=40)),
        sa.PrimaryKeyConstraint('id'),
        mysql_charset='utf8',
        mysql_engine='InnoDB'
    )


def downgrade():
    op.drop_table('user_roles')
    op.drop_table('roles')
    op.drop_table('users')

    op.drop_table('meals')
